### walmartProject

**Functionalities covered in this script:**

> Login to application. 
> Looks for search bar and button
> Retrieves embedded text from search bar and prints in console
> Clicking on Departments flyout
> Finds and clicks on `outdoor Living` category
> Finds and clicks on `SwimmingPool` sub category
> Finds and clicks on the item displayed in the pool items
> Validating if it redirects to respective item page with the item name
> Validating the quantity Less/More functionality
> Increasing the quantity from default 1 to 2
> Finds the price of item each and displaying it in console
> Adding the item to Cart
> Validating the success modal popup
> Asserting the success message in modal popup
> Asserting the item name in success modal popup
> Closing the Modal popup